#2. Escribir un guion que simule una calculadora básica

#!/bin/bash

if [ $# == 3 ] 
then
	case $2 in
		+) echo $(( $1 + $3 ));;
		-) echo $(( $1 - $3 ));;
		/) echo $(( $1 / $3 ));;
		X|x) echo $(( $1 * $3 ));;
		*) echo -e "No has introducido un parametro valido \n";;
	esac

else
	echo "Numero de parametros incorrecto"
fi