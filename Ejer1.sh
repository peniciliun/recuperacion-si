#1. Escribir un guión, pensado para ejecutarse al inicio de la sesión, que muestre un mensaje de
#saludo al usuario de manera que le desee Buenos días, buenas tardes o buenas noches según
#la hora del sistema. (1 puntos) Deberiamos meter el script en la carpeta etc y llamarlo motd

#!/bin/bash

user=`whoami`

hora=`date +%H`

if [ $hora -ge 0 ] && [ $hora -le 7 ];
	then
	echo "Buenas noches $user";
fi

if [ $hora -ge 8 ] && [ $hora -le 12 ];
	then
	echo "Buenos dias $user";
fi

if [ $hora -ge 13 ] && [ $hora -le 19 ];
	then
	echo "Buenas tardes $user";
fi

if [ $hora -ge 20 ] && [ $hora -le 23 ];
	then
	echo "Buenas noches $user";
fi