#!/bin/bash

echo "for 1"

for (( i=1 ; i<=5 ; i++ ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n "$i"
    done
    echo ""
done
echo ""

echo "for 2"

for (( i=1 ; i<=5 ; i++ ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n "$j"
    done
    echo ""
done
echo ""

echo "for 3"
for (( i=1 ; i<=5 ; i++ ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n " |"
    done
    echo "_ "
done
echo ""

echo "for 4"
for (( i=1 ; i<=5 ; i++ ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n " *"
    done
    echo ""
done

echo "for 5"
for (( i=1 ; i<=5 ; i++ ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n " *"
    done
    echo ""
done

for (( i=5 ; i>=1 ; i-- ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n " *"
    done
    echo ""
done
echo ""

echo "for 6"
for (( i=1 ; i<=3 ; i++ ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n "|Linux"
    done
    echo "______"
done

for (( i=3 ; i>=1 ; i-- ))
do
    for (( j=1 ; j<=i ;  j++ ))
    do
     echo -n "|Linux"
    done
    
    if [ $i == 3 ]; then
       echo -n "______"
       echo -ne ">> Powerd Server.\n"
    else
       echo "~~~~~"
    fi
done
echo ""

echo "for 7"
for (( i=1 ; i<=9 ; i++ ))
do
    for (( j=9 ; j>=i ; j-- ))
    do
       echo -n " "
    done
    for (( k=1 ; k<=i ;  k++ ))
    do
     echo -n " $i" 
    done
    echo ""
done
echo ""

echo "for 8"
for (( i=1 ; i<=9 ; i++ ))
do
    for (( j=9 ; j>=i ; j-- ))
    do
       echo -n " "
    done
    for (( k=1 ; k<=i ;  k++ ))
    do
     echo -n " ."
    done
    echo ""
done
echo ""

echo "for 9"
for (( i=1 ; i<=9 ; i++ ))
do
    for (( j=9 ; j>=i ; j-- ))
    do
       echo -n " "
    done
    for (( k=1 ; k<=i ;  k++ ))
    do
     echo -n " ." 
    done
    echo ""
done

for (( i=9 ; i>=1 ; i-- ))
do
    for (( j=i ; j<=9 ; j++ ))
    do
       echo -n " "
    done
    for (( k=1 ; k<=i ;  k++ ))
    do
     echo -n " ."
    done
    echo ""
done

echo ""
