#!/bin/bash

if [ $# == 1 ] 
then
	hoy=`date +"%Y %m %d %R"`
	busqueda=$1
	mkdir ~/"$hoy"
	ruta=~/"$hoy"
	echo "Introduzca su clave para activar el modo root"
	sudo -s
	for archivo in $(find /|grep $1) 
	do
		cp -r $archivo /"$ruta" 
	done
else
	echo "Numero de parametros incorrecto"
fi